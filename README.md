About
-----
This project is a tiny implementation of a transformer for personal learning purposes. The code is meant to be tiny and will be used as a module in other projects

Constraints
-----
To keep this project "small," this module will never surpass over 500 lines if that will even happen

### TODO
* Add Example Code of how this transformer could be used in various applications
